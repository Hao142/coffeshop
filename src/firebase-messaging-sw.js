
const firebase = require('firebase');
// <script type="module" src="firebase"></script>
if( 'undefined' === typeof window) {
  importScripts('https://www.gstatic.com/firebasejs/7.8.1/firebase-app.js');
  importScripts('https://www.gstatic.com/firebasejs/7.8.1/firebase-messaging.js');
}

const firebaseConfig = {
  apiKey: "AIzaSyC2sSGADD7ddBSqx-8-Tm1bjKc8VAFawr0",
    authDomain: "coffee-54304.firebaseapp.com",
    projectId: "coffee-54304",
    storageBucket: "coffee-54304.appspot.com",
    messagingSenderId: "169648405026",
    appId: "1:169648405026:web:677d0371bf5b81fac4a87b",
    measurementId: "G-DRVJV7F3JH"
}

const firebaseApp = firebase.initializeApp(firebaseConfig)
const messaging = firebaseApp.messaging();

messaging.onBackgroundMessage(function(payload) {
  console.log('Received background message ', payload);

  const notificationTitle = payload.notification.title;
  const notificationOptions = {
    body: payload.notification.body,
  };

  self.registration.showNotification(notificationTitle,
    notificationOptions);
});
