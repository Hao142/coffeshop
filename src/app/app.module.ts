import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { routing } from './app.routes';
import { MatButtonModule, MatRadioModule, MatInputModule, MatMenuModule, MatCheckboxModule } from '@angular/material';

import { AngularFireModule } from '@angular/fire';
import { AngularFireMessagingModule } from '@angular/fire/messaging';
import { MessagingService } from './services/messaging.service'
import { AsyncPipe } from '@angular/common'

import { AppComponent } from './app.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { HomeComponent } from './dashboard/home/home.component';
import { ProfileComponent } from './dashboard/profile/profile.component';
import 'hammerjs';
import { NavbarComponent } from './shared/navbar/navbar.component';
import { FigurecardComponent } from './shared/figurecard/figurecard.component';
import { ImagecardComponent } from './shared/imagecard/imagecard.component';
import { TableComponent } from './dashboard/table/table.component';
import { NotificationComponent } from './dashboard/notification/notification.component';
import { MsgIconBtnComponent } from './shared/msgiconbtn/msgiconbtn.component';
import { SweetAlertComponent } from './dashboard/sweetalert/sweetalert.component';
import { LoginComponent } from './page/login/login.component';
import { RootComponent } from './dashboard/root/root.component';
import { RegisterComponent } from './page/register/register.component';
import { LockComponent } from './page/lock/lock.component';
import { HeaderComponent } from './shared/header/header.component';
import { FooterComponent } from './shared/footer/footer.component';
import { SettingsComponent } from './dashboard/settings/settings.component';
import { PriceTableComponent } from './dashboard/component/pricetable/pricetable.component';
import { PanelsComponent } from './dashboard/component/panels/panels.component';

import { SettingsService } from './services/settings.service';
import { WizardComponent } from './dashboard/component/wizard/wizard.component';
import {HTTP_INTERCEPTORS, HttpClientJsonpModule, HttpClientModule} from '@angular/common/http';
import {HttpErrorInterceptor} from './services/http-interceptor.service';
import { DialogProductCategoryComponent } from './dashboard/component/dialog-productCategory/dialog-productCategory.component';
import {MatSelectModule} from '@angular/material/select';
import {MatNativeDateModule} from '@angular/material/core';
import {MatDialogModule, MatDialogRef} from '@angular/material/dialog';
import { ProductComponent } from './dashboard/product/product.component';
import {MatToolbarModule} from '@angular/material/toolbar';
import { DialogProductComponent } from './dashboard/component/dialog-product/dialog-product.component';
import {MatPaginatorModule} from '@angular/material/paginator';
import { DialogMailChimpComponent } from './dashboard/component/dialog-mail-chimp/dialog-mail-chimp.component';
import {MatCardModule} from '@angular/material/card';
import { environment } from 'environments/environment';
import { from } from 'rxjs';
import { SalesComponent } from './sales/sales.component';
import { DialogConfirmationComponent } from './dashboard/component/dialog-confirmation/dialog-confirmation.component';

export class MaterialModule { }

@NgModule({
  declarations: [
    AppComponent,
    SidebarComponent,
    HomeComponent,
    ProfileComponent,
    NavbarComponent,
    FigurecardComponent,
    ImagecardComponent,
    TableComponent,
    NotificationComponent,
    MsgIconBtnComponent,
    SweetAlertComponent,
    LoginComponent,
    RootComponent,
    RegisterComponent,
    LockComponent,
    HeaderComponent,
    FooterComponent,
    SettingsComponent,
    PriceTableComponent,
    PanelsComponent,
    WizardComponent,
    DialogProductCategoryComponent,
    DialogMailChimpComponent,
    DialogProductComponent,
    ProductComponent,
    DialogProductComponent,
    DialogMailChimpComponent,
    SalesComponent,
    DialogConfirmationComponent
  ],
    imports: [
        HttpClientModule,
        HttpClientJsonpModule,
        BrowserModule,
        FormsModule,
        MatDialogModule,
        HttpModule,
        routing,
        BrowserAnimationsModule,
        MatButtonModule,
        MatRadioModule,
        MatInputModule,
        MatMenuModule,
        MatCheckboxModule,
        MatSelectModule,
        MatToolbarModule,
        MatPaginatorModule,
        MatCardModule,
        AngularFireModule.initializeApp(environment.firebase),
        AngularFireMessagingModule
    ],
  entryComponents: [
    DialogProductCategoryComponent,
    DialogMailChimpComponent,
    DialogConfirmationComponent,
    DialogProductComponent
  ],
  providers: [
    AsyncPipe,
    {
      provide: MatDialogRef,
      useValue: {}
    },
    MatNativeDateModule,
    SettingsService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: HttpErrorInterceptor,
      multi: true,
    },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
