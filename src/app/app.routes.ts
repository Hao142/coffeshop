
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './dashboard/home/home.component';
import { ProfileComponent } from './dashboard/profile/profile.component';
import { TableComponent } from './dashboard/table/table.component';
import { NotificationComponent } from './dashboard/notification/notification.component';
import { SweetAlertComponent } from './dashboard/sweetalert/sweetalert.component';
import { SettingsComponent } from './dashboard/settings/settings.component';
import { PriceTableComponent } from './dashboard/component/pricetable/pricetable.component';
import { PanelsComponent} from './dashboard/component/panels/panels.component';
import { WizardComponent } from './dashboard/component/wizard/wizard.component';

import { RootComponent } from './dashboard/root/root.component';
import { LoginComponent } from './page/login/login.component';
import { LockComponent } from './page/lock/lock.component';
import { RegisterComponent } from './page/register/register.component';
import {AuthGuard} from './helper/auth.guard';
import {ProductComponent} from './dashboard/product/product.component';
import { SalesComponent } from './sales/sales.component';

const routes: Routes = [
  {path: '',
    redirectTo: 'ecommerce',
    pathMatch: 'full',
  },
  {path: 'lock', component: LockComponent, canActivate: [AuthGuard]},
  {path: 'register', component: RegisterComponent, canActivate: [AuthGuard]},
  {path: 'dashboard', component: RootComponent, children: [
    {path: '', component: SalesComponent},
    {path: 'profile', component: ProfileComponent},
    {path: 'table', component: TableComponent},
      {path: 'table/product', component: ProductComponent},
    {path: 'notification', component: NotificationComponent},
    {path: 'alert', component: SweetAlertComponent},
    {path: 'settings', component: SettingsComponent},
    {path: 'components/price-table', component: PriceTableComponent},
    {path: 'components/panels', component: PanelsComponent},
    {path: 'components/wizard', component: WizardComponent}
  ], canActivate: [AuthGuard]},
  {path: 'login', component: LoginComponent},
  {path: 'ecommerce',
    loadChildren: () => import('./ecommerce/ecommerce.module').then(m => m.EcommerceModule),
  }
];

export const routing = RouterModule.forRoot(routes);

