import { Component, OnInit } from '@angular/core';
import {AccountService} from '../../services/account.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  userName: string
  passWord: string
  private submitted: boolean;

  constructor(
    private accountService: AccountService,
  private router: Router,
  ) { }

  ngOnInit() {
  }

  loginBtn() {
    this.submitted = true;
    this.accountService.login(this.userName, this.passWord);
    this.router.navigate(['/dashboard']);
  }
}
