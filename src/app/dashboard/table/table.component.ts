import { Component, OnInit } from '@angular/core';
import {MatDialog} from '@angular/material/dialog';
import {DialogProductCategoryComponent} from '../component/dialog-productCategory/dialog-productCategory.component';
import {CategoryApiService} from '../../services/category-api.service';
import {DialogConfirmationComponent} from '../component/dialog-confirmation/dialog-confirmation.component';
declare interface TableData {
  headerRow: any[];
  dataRows: any[];
}
@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.css']
})
export class TableComponent implements OnInit {
  public tableData1: TableData;
  constructor(public dialog: MatDialog,
              public categoriesService: CategoryApiService) { }

  ngOnInit() {

    this.tableData1 = {
      headerRow: [{ name: 'Name', type: 'Type', note: 'note', price: 'Price', action: 'Action'}],
      dataRows: []
    };
    this.categoriesService.get_list().subscribe((data: any) => {
        this.tableData1.dataRows = data.result;
    })
  }

  openDialogAdd(item?: any): void {
    this.openDialog('Add', item);
  }

  openDialogEdit(item?: any): void {
    this.openDialog('Edit', item);
  }

  addCategory(item: any): void {
      this.categoriesService.add(item).subscribe((data: any) => {
          window.location.reload();
          console.log('categoryItem: ', data);
      })
  }

  editCategory(item: any): void {
    this.categoriesService.add(item).subscribe((data: any) => {
      window.location.reload();
      console.log('categoryItem: ', data);
    })
  }

  deleteItem(item?: any): void {
    const dialogRef = this.dialog.open(DialogConfirmationComponent, {
      width: '600px',
      data: {item: item}
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        if (result.event === ' No') {
          console.log('event: ', result.event);
        } else if (result.event === 'Yes') {
          this.categoriesService.delete(item.id).subscribe((p) => {
            console.log(p);
          }, error => {
            console.log('error: ', error);
          }, () => {
          });
        }
      }
    });
  }


  openDialog(action: any, obj: any) {
    const dialogRef = this.dialog.open(DialogProductCategoryComponent, {
      width: '300px',
      data: { item: obj, action: action }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        if (result.event === 'Add') {
          this.addCategory(result.data);
        }
        if (result.event === 'Edit') {
          this.editCategory(result.data);
        }
      }
    });
  }



}
