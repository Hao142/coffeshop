import {Component, OnInit, OnDestroy, ElementRef} from '@angular/core';
import { SettingsService } from '../../services/settings.service';
import {Subscription} from 'rxjs';
import {first} from 'rxjs/operators';

@Component({
  selector: 'app-root',
  templateUrl: './root.component.html',
  styleUrls: ['./root.component.css']
})
export class RootComponent implements OnInit, OnDestroy {
  public id: number;
  public backgroundColor: string;
  public subscriptionGroup: Subscription[] = [];
  public subscription1: Subscription;
  public subscription2: Subscription;
  constructor(public settingService: SettingsService, private elementRef: ElementRef) {
    this.id = 1;
    this.backgroundColor = '#D80B0B'
  }

  ngOnInit() {
    console.log('ngOnInit');
    if (!this.subscription1) {
      this.subscription1 = this.settingService.sidebarImageIndexUpdate.pipe(first()).subscribe((id: number) => {
        this.id = id + 1;
      });
      this.subscriptionGroup.push(this.subscription1);
    }
    if (this.subscription2) {
      this.subscription2 = this.settingService.sidebarColorUpdate.pipe(first()).subscribe((color: string) => {
        this.backgroundColor = color;
      });
      this.subscriptionGroup.push(this.subscription2);
    }
  }

  ngOnDestroy() {
    console.log('ngOnDestroy: ');
    this.subscriptionGroup.forEach(subscription => {
      subscription.unsubscribe()
    })
  }
}
