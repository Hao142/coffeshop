import { Component, OnInit } from '@angular/core';
import {ProductAPIService} from '../../services/product-api.service';
import {ActivatedRoute} from '@angular/router';

import {MatDialog} from '@angular/material/dialog';
import {DialogProductComponent} from '../component/dialog-product/dialog-product.component';
import {ThirdpartyService} from '../../services/thirdparty.service';
import {PageEvent} from '@angular/material/paginator';
import {DialogConfirmationComponent} from '../component/dialog-confirmation/dialog-confirmation.component';
declare interface TableData {
  headerRow: any[];
  dataRows: any[];
}
@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css']
})
export class ProductComponent implements OnInit {
  public pageSize = 5;
  public pageIndex = 0;
  public pageEvent: PageEvent;
  public length = 10;
  public pageSizeOptions: number[] = [5, 10, 25, 100];
  public tableData1: TableData;
  private category_id: any;


  constructor(
    public dialog: MatDialog,
    private productService: ProductAPIService,
    private thirdpartyService: ThirdpartyService,
    private activeRoute: ActivatedRoute,
  ) { }

  ngOnInit() {
    this.activeRoute.queryParams.subscribe(params => {
      this.category_id = params.category_id;
      console.log(params);
    });
    this.tableData1 = {
      headerRow: [{ name: 'Name', type: 'Type', content: 'Content', price: 'Price', action: 'Action'}],
      dataRows: [
      ]
    };
    this.productService.get_list(1, this.category_id, this.pageSize).subscribe((data: any) => {
      this.tableData1.dataRows = data.result.data;
      console.log('data: ', data);
    })
  }

  public getServerData(event?: any) {
    this.productService.get_list(event.pageIndex + 1, this.category_id, event.pageSize)
      .subscribe((data: any) => {
        console.log('data: ', data);
        this.tableData1.dataRows = [];
        this.length = data.result.total;
        this.tableData1.dataRows.push(...data.result.data);
        console.log(' this.tableData1.dataRows: ',  this.tableData1.dataRows);
      });
    return event;
  }

  openDialogAdd(item?: any): void {
    this.openDialog('Add', item, this.category_id);
  }

  openDialogEdit(item?: any): void {
    console.log('DialogEditProduct')
    this.openDialog('Edit', item, this.category_id);
  }

  deleteItem(item?: any): void {
    const dialogRef = this.dialog.open(DialogConfirmationComponent, {
      width: '600px',
      data: {item: item}
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        if (result.event === ' No') {
          console.log('event: ', result.event);
        } else if (result.event === 'Yes') {
          this.productService.delete(item.id).subscribe((p) => {
            console.log(p);
          }, error => {
            console.log('error: ', error);
          }, () => {
          });
        }
      }
    });
  }

  // deleteItem(item?: any): void {
  //   console.log('item', item);
  //   this.productService.delete(item.id).subscribe((data: any) => {
  //     if (data.code === 200) {
  //       console.log(data);
  //       window.location.reload();
  //     }
  //   })
  // }

  addItem(item?: any): void {
    console.log('Additem: ', item);
    const formData = new FormData();
    item.avatar = item.avatar.replace('data:image/jpeg;base64,', '');
    formData.append('image', item.avatar);
    this.thirdpartyService.postImage(formData).subscribe((returnData: any) => {
        console.log('return data', returnData);
      item.avatar = returnData.data.link;
    }, () => {

    }, () => {
      this.productService.add(item).subscribe((data) => {
        if (data.code === 200) {
          console.log(data);
          window.location.reload();
        }
      })
    })
  }

  editItem(item?: any, ifImageChange?: any): void {
    const formData = new FormData();
    item.avatar = item.avatar.replace('data:image/jpeg;base64,', '');
    formData.append('image', item.avatar);
    if (ifImageChange === true) {
      this.thirdpartyService.postImage(formData).subscribe((returnData: any) => {
        item.avatar = returnData.data.link;
      }, () => {

      }, () => {
        this.productService.update(item).subscribe((data) => {
          if (data.code === 200) {
            console.log(data);
            window.location.reload();
          }
        })
      })
    } else if (ifImageChange === false) {
      console.log('return data', item);
      this.productService.update(item).subscribe((data) => {
        if (data.code === 200) {
            console.log(data);
            window.location.reload();
        }
      })
    }
  }

  openDialog(action: any, obj: any, category_id?: any) {
    const dialogRef = this.dialog.open(DialogProductComponent, {
      width: '600px',
      data: { item: obj, action: action, category_id: category_id }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        if (result.event === 'Add') {
          console.log('resultDialog: ', result);
          this.addItem(result.data);
        }
        if (result.event === 'Edit') {
          this.editItem(result.data, result.ifImageChange);
        }
      }
    });
  }

}
