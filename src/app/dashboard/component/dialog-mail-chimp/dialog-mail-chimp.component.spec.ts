import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogMailChimpComponent } from './dialog-mail-chimp.component';

describe('DialogMailChimpComponent', () => {
  let component: DialogMailChimpComponent;
  let fixture: ComponentFixture<DialogMailChimpComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DialogMailChimpComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogMailChimpComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
