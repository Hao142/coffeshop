import { Component, OnInit } from '@angular/core';
import {NgForm} from '@angular/forms';
import {MailChimpSubcribeServiceService} from '../../../services/mail-chimp-subcribe-service.service';
import {DialogProductComponent} from '../dialog-product/dialog-product.component';
import {MatDialog} from '@angular/material/dialog';

@Component({
  selector: 'app-dialog-mail-chimp',
  templateUrl: './dialog-mail-chimp.component.html',
  styleUrls: ['./dialog-mail-chimp.component.css']
})
export class DialogMailChimpComponent implements OnInit {
  subscribeData: any = <any>{};
  constructor(
    private subscribeService: MailChimpSubcribeServiceService
  ) { }
  ngOnInit() {
  }



  subscribe(subscribeForm: NgForm) {
    if (subscribeForm.invalid) {
      return;
    }
    this.subscribeService.subscribeToList(this.subscribeData)
      .subscribe(res => {
        alert('Subscribed!');
      }, err => {
        console.log('this.subscribeData: ', this.subscribeData)
        console.log(err);
      })
  }
}
