import {ChangeDetectorRef, Component, Inject, OnInit, Optional, ViewChild} from '@angular/core';
import {FormControl} from '@angular/forms';
import {ReplaySubject, Subject} from 'rxjs';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {DataSource} from '@angular/cdk/collections';




@Component({
  selector: 'app-dialog-product',
  templateUrl: './dialog-productCategory.component.html',
  styleUrls: ['./dialog-productCategory.component.css']
})

//

export class DialogProductCategoryComponent implements OnInit {

  action: string;
  local_data: any;
  length = 10;
  pageSize = 5;
  pageIndex = 0;
  pageSizeOptions: number[] = [5, 10, 25, 100];
  itemTypeList: any;

  constructor(
    public dialogRef: MatDialogRef<DialogProductCategoryComponent>,
    // @Optional() is used to prevent error if no data is passed
    @Optional() @Inject(MAT_DIALOG_DATA) public data: any) {
    // this.myDate = this.datepipe.transform(this.myDate, 'yyyy-MM-dd');
    this.local_data = { ...data.item };
    console.log('this.local_data', this.local_data);
    this.action = this.data.action;
    // this.itemtypeService.get_list({ pageIndex: this.pageIndex, pageSize: this.pageSize, length: this.length }).subscribe((result) => {
    //   this.itemTypeList = result.content;
    //   console.log('result: ', this.itemTypeList);
    // });
  }

  ngOnInit(): void {
    console.log('dialog-info');
    if (this.action === 'Add') {
    }
  }


  doAction() {
    this.dialogRef.close({ event: this.action, data: this.local_data });
  }

  closeDialog() {
    this.dialogRef.close({ event: 'Cancel' });
  }
}
