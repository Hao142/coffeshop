import {Component, Inject, OnInit, Optional} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
@Component({
  selector: 'app-dialog-confirmation',
  templateUrl: './dialog-confirmation.component.html',
  styleUrls: ['./dialog-confirmation.component.css']
})
export class DialogConfirmationComponent implements OnInit {

  action: string;
  local_data: any;
  length = 10;
  pageSize = 5;
  pageIndex = 0;

  ngOnInit(): void {
    console.log('dialog-info');
    if (this.action === 'Add') {
    }
  }

  constructor(
    public dialogRef: MatDialogRef<DialogConfirmationComponent>,
    @Optional() @Inject(MAT_DIALOG_DATA) public data: any) {
    this.local_data = { ...data.item };
    console.log('this.local_data', this.local_data);
    this.action = this.data.action;
  }

  onDismiss() {
    this.dialogRef.close({ event: 'No' });
  }

  onConfirm() {
    this.dialogRef.close({ event: 'Yes' });
  }

}
