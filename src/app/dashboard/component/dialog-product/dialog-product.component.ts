import {ChangeDetectorRef, Component, ElementRef, Inject, OnInit, Optional, ViewChild} from '@angular/core';
import {FormControl} from '@angular/forms';
import {ReplaySubject, Subject} from 'rxjs';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {DataSource} from '@angular/cdk/collections';




@Component({
  selector: 'app-dialog-product',
  templateUrl: './dialog-product.component.html',
  styleUrls: ['./dialog-product.component.css']
})

//

export class DialogProductComponent implements OnInit {
  ifImageChange = false;
  action: string;
  local_data: any;
  category_id?: any;
  length = 10;
  pageSize = 5;
  pageIndex = 0;
  pageSizeOptions: number[] = [5, 10, 25, 100];
  itemTypeList: any;

  @ViewChild('fileInput',{static: true}) fileInput: ElementRef;
  fileAttr = 'Choose File';

  constructor(
    public dialogRef: MatDialogRef<DialogProductComponent>,
    // @Optional() is used to prevent error if no data is passed
    @Optional() @Inject(MAT_DIALOG_DATA) public data: any) {
    this.local_data = { ...data.item };
    this.category_id = data.category_id;

    console.log('this.local_data', this.category_id);
    this.action = this.data.action;
  }

  ngOnInit(): void {
    console.log('dialog-info');
    if (this.action === 'Add') {
    }
  }

  onOptionsSelected(event) {
    const itemType = this.itemTypeList.find(x => x.type === event.value);
    console.log('itemType: ', itemType);
    this.local_data.checkListItemType =  itemType;
  }

  uploadFileEvt(imgFile: any) {
    // WIP: will be implemented later on
    this.ifImageChange = true;
    if (imgFile.target.files && imgFile.target.files[0]) {
      this.fileAttr = '';
      Array.from(imgFile.target.files).forEach((file: File) => {
        this.fileAttr += file.name ;
      });

      // HTML5 FileReader API
      const reader = new FileReader();
      reader.onload = (e: any) => {
        const image = new Image();
        image.src = e.target.result;
        image.onload = rs => {
          const imgBase64Path = e.target.result;
          console.log(imgBase64Path);
          this.local_data.avatar = imgBase64Path;
        };
      };
      reader.readAsDataURL(imgFile.target.files[0]);

      // Reset if duplicate image uploaded again
      this.fileInput.nativeElement.value = '';
    } else {
      this.fileAttr = 'Choose File';
    }
  }


  doAction() {
    this.local_data.category_id = this.category_id;
    this.dialogRef.close({ event: this.action, data: this.local_data, ifImageChange: this.ifImageChange });
  }

  closeDialog() {
    this.dialogRef.close({ event: 'Cancel' });
  }
}
