import { Component, OnInit } from '@angular/core';
import { MessagingService } from 'app/services/messaging.service';

@Component({
  selector: 'app-sales',
  templateUrl: './sales.component.html',
  styleUrls: ['./sales.component.css']
})
export class SalesComponent implements OnInit {
  Object = Object;
  orderList=[]
  constructor(private messagingService: MessagingService) { }

  ngOnInit() {
    let orderItems;
    try{
      orderItems = JSON.parse(localStorage.getItem('ordersItem'))
      console.log({orderItems})
    }catch(e){}
    if(!Array.isArray(orderItems)){
      orderItems = [];
    }
    this.orderList = orderItems.sort((a,b)=>a.status-b.status)
    this.messagingService.requestPermission();
    this.messagingService.listen();
    this.messagingService.currentMessage.subscribe(message=>{
      if(message && message.data){
        message.data.products = JSON.parse(message.data.products);
        if(message.data.status==1){
          this.orderList.splice(0,0,message.data)
        }
      }
    })
  }
  log(data){
    console.log({data})
  }
  updateState(order,status){
    console.log("update:",this.orderList)
    order.status=status;
    this.messagingService.sendMessage(order.firebaseToken, order).subscribe(()=>{
      localStorage.setItem('ordersItem',JSON.stringify(this.orderList));
    });
    setTimeout(() => {
      this.orderList.sort((a,b)=>a.status-b.status)
    }, 5000);
  }
}
