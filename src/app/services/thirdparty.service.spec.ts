import { TestBed } from '@angular/core/testing';

import { ThirdpartyService } from './thirdparty.service';

describe('ThirdpartyService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ThirdpartyService = TestBed.get(ThirdpartyService);
    expect(service).toBeTruthy();
  });
});
