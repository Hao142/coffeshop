import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})


export class ThirdpartyService {
  constructor(private http: HttpClient) { }
  postImage(item?: any) {
    const headers = {'authorization': 'Client-ID ea5fb44404ad459'};
    return this.http.post(`https://api.imgur.com/3/image`, item, {headers: headers});
  }

}
