import { Injectable } from '@angular/core';
import {environment} from '../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserAPIService {
  apiUrl = environment.categoryApi;

  constructor(private http: HttpClient) { }

  get_list(page?: any): Observable<any> {
    return this.http.get<any>(`${this.apiUrl}/list-item?page=` + (page.pageIndex + 1) + `&limit=` + page.pageSize);
  }

  add(item?: any): Observable<any> {
    return this.http.post<any>(`${this.apiUrl}/update-category`, item);
  }

  update(item?: any): Observable<any> {
    return this.http.post<any>(`${this.apiUrl}/update-category`, item);
  }

  delete(id?: any) {
    return this.http.delete(`${this.apiUrl}/delete-category`, id);
  }
}
