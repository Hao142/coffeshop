import { TestBed } from '@angular/core/testing';

import { CategoryAPIService } from './category-api.service';

describe('CategoryAPIService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CategoryAPIService = TestBed.get(CategoryAPIService);
    expect(service).toBeTruthy();
  });
});
