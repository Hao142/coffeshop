import {Injectable, OnInit} from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import {state} from '@angular/animations';
import {RootComponent} from '../dashboard/root/root.component';
import {SettingsService} from './settings.service';


@Injectable({ providedIn: 'root' })
export class AccountService{

  public userValue: any;

  constructor(
    private settingService: SettingsService,
    private router: Router,
  ) {
  }


  public login(userName: any, passWord: any) {
      if (userName === 'admin' && passWord === 'admin') {
          const value = {
              username : userName,
              passWord : passWord,
          }
          localStorage.setItem('user', JSON.stringify(value));
        this.router.navigate(['/dashboard']);
      }
  }

  public getUserValue(): any {
      return this.userValue = JSON.parse(localStorage.getItem('user'));
  }

  logout() {
    localStorage.removeItem('user');
    this.router.navigate(['/login']);
  }

}
