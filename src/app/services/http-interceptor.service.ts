import {
  HttpEvent,
    HttpInterceptor,
    HttpHandler,
    HttpRequest,
    HttpResponse,
    HttpErrorResponse
} from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';

export class HttpErrorInterceptor implements HttpInterceptor {
  // clientSecret: '11c2d6269f69fbc2271559492a0824bc4e9b0b1e'
  // Adding authorized header to each call
  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
  //   request = request.clone({
  //     setHeaders: {
  //       'Content-Type' : 'multipart/form-data',
  //       'Accept'       : '*/*',
  //       'Authorization': `Client-ID ${this.clientSecret}`,
  //     },
  //   });
    return next.handle(request)
      .pipe(
        retry(1),
        catchError((error: HttpErrorResponse) => {
          let errorMessage = '';
          if (error.error instanceof ErrorEvent) {
            // client-side error
            errorMessage = `Error: ${error.error.message}`;
          } else {
            // server-side error
            errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
          }
          console.log({errorMessage});
          return throwError(errorMessage);
        })
      )
  }
}
