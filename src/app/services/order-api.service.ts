import { Injectable } from '@angular/core';
import {Observable} from 'rxjs';
import {environment} from '../../environments/environment';
import {Http} from '@angular/http';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class OrderApiService {
  apiUrl = environment.orderApi;

  constructor(private http: HttpClient) { }

  get_list(page?: any, category_id?: any, length?: any): Observable<any> {
    return this.http.get<any>(`${this.apiUrl}?page=` + (page) + `&category=` + category_id + `&length=` + length);
  }

  get(id: any): Observable<any> {
    return this.http.get<any>(`${this.apiUrl}/${id}`);
  }

  add(item?: any): Observable<any> {
    return this.http.post<any>(`${this.apiUrl}`, item);
  }

  update(item?: any): Observable<any> {
    return this.http.put<any>(`${this.apiUrl}`, item);
  }

  delete(id?: any) {
    return this.http.delete(`${this.apiUrl}/${id}` );
  }
}
