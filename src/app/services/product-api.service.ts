import { Injectable } from '@angular/core';
import {environment} from '../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ProductAPIService {
  apiUrl = environment.productApi;

  constructor(private http: HttpClient) { }

  get_list(page?: any, category_id?: any, length?: any): Observable<any> {
    return this.http.get<any>(`${this.apiUrl}?page=` + (page) + `&category=` + category_id + `&length=` + length);
  }

  get(id: any): Observable<any> {
    return this.http.get<any>(`${this.apiUrl}/${id}`);
  }

  add(item?: any): Observable<any> {
    return this.http.post<any>(`${this.apiUrl}`, item);
  }

  update(item?: any): Observable<any> {
    return this.http.put<any>(`${this.apiUrl}`, item);
  }

  delete(id?: any) {
    return this.http.delete(`${this.apiUrl}/${id}` );
  }

}
