import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class MailChimpSubcribeServiceService {
  mailChimpEndpoint = 'https://telus.us3.list-manage.com/subscribe/post-json?u=dad60fb19238466b28e69577e&id=18332a2f74';
  constructor(
    private http: HttpClient
  ) { }
  
  // dad60fb19238466b28e69577e
  // 18332a2f74
  // https://gmail.us1.list-manage.com/unsubscribe?u=dad60fb19238466b28e69577e&id=18332a2f74
  subscribeToList(data) {
    const params = new HttpParams()
      .set('FNAME', data.firstName)
      .set('LNAME', data.lastName)
      .set('EMAIL', data.email)
      .set('group[377690][1]', 'true')
      .set('b_dad60fb19238466b28e69577e_18332a2f74', '');
    const mailChimpUrl = `${this.mailChimpEndpoint}&${params.toString()}`;
    return this.http.jsonp(mailChimpUrl, 'c')
  }
}
