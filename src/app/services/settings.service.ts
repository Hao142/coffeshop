import { Injectable, EventEmitter } from '@angular/core';
import {BehaviorSubject, Observable} from 'rxjs';

@Injectable()
export class SettingsService {
  public sidebarImageIndex = 0;
  public sidebarImageIndexUpdate = new BehaviorSubject<number>(0);
  public sidebarFilter = '#fff';
  public sidebarFilterUpdate = new BehaviorSubject<string>('#fff');
  public sidebarColor = '#D80B0B';
  public sidebarColorUpdate = new BehaviorSubject<string>('#D80B0B');

  constructor() { }

  getSidebarImageIndex(): number {
    return this.sidebarImageIndex;
  }
  setSidebarImageIndex(id) {
    this.sidebarImageIndex = id;
    this.sidebarImageIndexUpdate.next(this.sidebarImageIndex);
  }
  getSidebarFilter(): string {
    return this.sidebarFilter;
  }
  setSidebarFilter(color: string) {
    this.sidebarFilter = color;
    this.sidebarFilterUpdate.next(this.sidebarFilter);
  }
  getSidebarColor(): string {
    return this.sidebarColor;
  }
  setSidebarColor(color: string) {
    this.sidebarColor = color;
    this.sidebarColorUpdate.next(this.sidebarColor);
  }
}
