import { Injectable } from '@angular/core';
import { AngularFireMessaging } from '@angular/fire/messaging';
import * as firebase from 'firebase';
import { BehaviorSubject } from 'rxjs'
import { mergeMapTo } from 'rxjs/operators';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class MessagingService {
  token = new BehaviorSubject(null);
  currentMessage = new BehaviorSubject(null);

  constructor(private angularFireMessaging: AngularFireMessaging, private http: HttpClient) {}

  async requestPermission() {
    await this.angularFireMessaging.requestPermission
      .pipe(mergeMapTo(this.angularFireMessaging.tokenChanges))
      .subscribe(
        (token) => {
          console.log('Permission granted! Save to the server!', token);
          this.token.next(token)
        },
        (error) => { console.error(error); },
      );
  }

  async listen() {
    await this.angularFireMessaging.messages
      .subscribe((message) => {
        console.log("new message:", message)
        this.currentMessage.next(message)
      })
  }

  sendMessage(token: string, data: any) {
    return this.http.post(
      'https://fcm.googleapis.com/fcm/send',
      {
        'data': data,
        'to': token
      },
      {
        headers: {
          'Content-Type': 'application/json',
          'Authorization': 'key=AAAAJ3_VOiI:APA91bFUTGoDfGDZFet8Z4pmn8Eg6aP49T84ZqzP0g5kRFECN_tfjCjf1cv5W93LOn2BVk1erPq6nOqQsEim_PfNTyM-BrT39wx4Jy8LBGBg1n8FBqylkz5GmLONFjBxSQi8NmDbl6fR'
        }
      }
    )
  }

}
