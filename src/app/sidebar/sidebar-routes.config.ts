export const ROUTES = [
    { path: '/dashboard', title: 'Orders List', icon: 'dashboard', children: null },
    { path: 'profile', title: 'User Profile', icon: 'person', children: null },
    { path: 'table', title: 'Product Category', icon: 'content_paste', children: null },
    // { path: '#component', id: 'component', title: 'Product', icon: 'apps', children: [
    //     {path: 'components/price-table', title: 'ProductCatetegory', icon: 'PT'},
    //     {path: 'components/panels', title: 'Panels', icon: 'P'},
    //     {path: 'components/wizard', title: 'Wizard', icon: 'W'},
    //   ]},
    { path: 'notification', title: 'Notification', icon: 'notifications', children: null },
    { path: 'alert', title: 'Sweet Alert', icon: 'warning', children: null },
    { path: 'settings', title: 'Settings', icon: 'settings', children: null },
];
