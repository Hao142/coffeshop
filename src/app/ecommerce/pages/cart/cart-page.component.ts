/**
 * Created by andrew.yang on 7/31/2017.
 */
import {Component, OnDestroy, OnInit} from '@angular/core';
import {CartBaseComponent} from './cart-base.component';
import {CartService} from '../../services/cart.service';
import {OrderApiService} from '../../../services/order-api.service';
import {forEach} from '@angular/router/src/utils/collection';
import {Router} from '@angular/router';
import { MessagingService } from 'app/services/messaging.service';
import { isArray } from 'util';

@Component({
    selector: 'app-cart-page',
    styleUrls: ['cart-page.component.css'],
    templateUrl: 'cart-page.component.html'
})
export class CartPageComponent extends CartBaseComponent implements OnInit, OnDestroy {
  orderObject: any = {}
  firebaseToken;
  constructor(
    private router: Router,
    protected cartService: CartService,
    private orderSerivce: OrderApiService,
    private messagingService: MessagingService
  ) {
    super(cartService);
  }

    ngOnInit() {
        this.cartList = JSON.parse(sessionStorage.getItem('cartItem'));
        this.messagingService.requestPermission();
        this.messagingService.token.subscribe(data => this.firebaseToken = data);
    }

    changeQuantity = (cart, quantity) => {
        cart.quantity = quantity;
        console.log('cartWhen', this.cartList);
        this.cartService.reloadCart(this.cartList);
    }

    buildOrderBody(current: any): any {
      const products = [];
      current.forEach((data) => {
        const product = {
            id : data.product.id,
            name : data.product.name,
            photo : data.product.avatar || data.product.image,
            number : data.quantity
          }
          products.push(product);
      })
      this.orderObject.products = products;
      this.orderObject.status = 1;
      this.orderObject.firebaseToken = this.firebaseToken;
      this.orderObject.__id = Date.now();
    }

    ngOnDestroy() {
      this.cartService.cartListSubject.unsubscribe();
    }

  async checkout() {
        const current = JSON.parse(sessionStorage.getItem('cartItem'));
        console.log(current)
        this.buildOrderBody(current);
        console.log("order", this.orderObject)
        this.messagingService.sendMessage("fsI4tcqE0HQrnNHLKFKo-f:APA91bFhp8i7DCxRBQD24ry0mkeUVvB4jfxQqvXYviLJwuUmGCaClfG3JlTkFVMVK7U4z-Njt1BsQNQpZ6q-OIymyuutmPBA2h-92FdMceTqJDXmDyWiLzIJfmzANBdky_q1lu9N8DRz",
          this.orderObject
        ).subscribe(()=>{

          sessionStorage.removeItem('cartItem');
          let orderList
          try{
            orderList = JSON.parse(localStorage.getItem('ordersItem'));
          }catch(e){}
          if(!Array.isArray(orderList)){
            orderList = []
          }
          orderList.splice(0,0,this.orderObject)
          localStorage.setItem('ordersItem',JSON.stringify(orderList))
          this.router.navigate(['/ecommerce/order'])
        })
    }
}
