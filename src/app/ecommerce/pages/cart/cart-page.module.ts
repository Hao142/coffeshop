/**
 * Created by andrew.yang on 7/31/2017.
 */

import {NgModule} from "@angular/core";
import {SharedModule} from "../../shared/shared.module";
import {RouterModule} from "@angular/router";
import {CartPageComponent} from "./cart-page.component";
import {cartPageRoutes} from "./cart-page.routes";
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';
import { MessagingService } from 'app/services/messaging.service'
@NgModule({
  imports: [
    SharedModule,
    RouterModule.forChild(cartPageRoutes),
    MatFormFieldModule,
    MatInputModule
  ],
  declarations: [
      CartPageComponent
  ],
  providers:[
    MessagingService
  ]
})
export class CartPageModule { }
