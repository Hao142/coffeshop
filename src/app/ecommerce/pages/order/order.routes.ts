/**
 * Created by andrew.yang on 7/27/2017.
 */
 import {OrderComponent} from './order.component';

 export const orderRoutes = [
     {
         path: '',
         component: OrderComponent
     },
 ];
