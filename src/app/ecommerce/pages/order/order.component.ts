import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { MessagingService } from 'app/services/messaging.service';
import { isArray } from 'util';

@Component({
  selector: 'app-order',
  templateUrl: './order.component.html',
  styleUrls: ['./order.component.css']
})
export class OrderComponent implements OnInit {
  Object = Object;
  orderList=[]
  constructor(private messagingService: MessagingService, private route: ActivatedRoute) { }

  ngOnInit() {
    this.messagingService.requestPermission();
    this.messagingService.listen();
    let orderItems;
    try{
      orderItems = JSON.parse(localStorage.getItem('ordersItem'))
      console.log({orderItems})
    }catch(e){}
    if(!Array.isArray(orderItems)){
      orderItems = [];
    }
    this.orderList = orderItems.sort((a,b)=>b.__id-a.__id)
    this.messagingService.currentMessage.subscribe(message=>{
      if(message && message.data){
        message.data.products = JSON.parse(message.data.products);
        if(message.data.status!=1){
          let Iorder = this.orderList.find(order=>order.__id==message.data.__id)
          if(Iorder){
            Iorder.status=message.data.status;
          }else{
            console.log('cannot find:',message.data.__id)
          }
        }
      }
    })
  }

}
