/**
 * Created by andrew.yang on 7/27/2017.
 */
 import {NgModule} from '@angular/core';
 import {RouterModule} from '@angular/router';
 import {orderRoutes} from './order.routes';
 import {OrderComponent} from './order.component';
import { SharedModule } from 'app/ecommerce/shared/shared.module';
import { MessagingService } from 'app/services/messaging.service';
 @NgModule({
     imports: [
        SharedModule,
        RouterModule.forChild(orderRoutes)
     ],
     declarations: [
        OrderComponent
     ],
     providers:[
       MessagingService
     ]
 })
 export class OrderModule { }
