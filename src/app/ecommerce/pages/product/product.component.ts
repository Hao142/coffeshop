import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {ProductService} from '../../services/products.service';
import {Product} from '../../model/product';
import {CartService} from '../../services/cart.service';
import {ProductAPIService} from '../../../services/product-api.service';

@Component({
    selector: 'app-product',
    templateUrl: './product.component.html',
    styleUrls: ['./product.component.css']
})
export class ProductComponent implements OnInit, OnDestroy {
    public product: any;
    product_id: any;
    quantity = 1;
    constructor(private route: ActivatedRoute,
                private productService: ProductAPIService,
                private cartService: CartService
    ) { }

    ngOnInit() {
      this.route.queryParams.subscribe(params => {
        console.log(params);
        this.product_id = params.product_id;
      });
       this.productService.get(this.product_id).subscribe((data) => {
            console.log('data', data);
            this.product = data.result;
       });
    }

    changeQuantity = (newQuantity: number) => {
        this.quantity = newQuantity;
    };
    addToCart = (product) => {
        if (this.quantity) { this.cartService.addToCart({product, quantity: this.quantity}) }
    };

    ngOnDestroy() {

    }
}
