import {Component, OnDestroy, OnInit} from '@angular/core';
import {ProductService} from '../../services/products.service';
import {Product} from '../../model/product';
import {CartService} from '../../services/cart.service';
import {Router} from '@angular/router';
import {ProductAPIService} from '../../../services/product-api.service';
import {MiddleServiceService} from '../../services/middle-service.service';
import {DialogProductComponent} from '../../../dashboard/component/dialog-product/dialog-product.component';
import {MatDialog} from '@angular/material/dialog';
import {DialogMailChimpComponent} from '../../../dashboard/component/dialog-mail-chimp/dialog-mail-chimp.component';

@Component({
    selector: 'app-category',
    templateUrl: './category.component.html',
    styleUrls: ['./category.component.css']
})


export class CategoryComponent implements OnInit, OnDestroy {
    products: any[];
    page = 1;
    constructor(
         private dialog: MatDialog,
         private productService: ProductAPIService,
         private cartService: CartService,
         private middleService: MiddleServiceService,
         private router: Router
    ) { }

    ngOnInit() {
      this.productService.get_list(this.page, 11).subscribe((data) => {
            this.products = data.result.data;
            this.openDialog();
      });

      // Fetching data from TopBarComponent
      let categoryId;
      this.middleService.SharingData.subscribe((data:any) => {
        console.log('receiveData: ', data);
        categoryId = data.id;
        this.productService.get_list(this.page, categoryId).subscribe((dataProduct) => {
          this.products = dataProduct.result.data;
        });
      });
    }

    addToCart = (product) => {
        this.cartService.addToCart({product, quantity: 1})
    };

  openDialog() {
    const dialogRef = this.dialog.open(DialogMailChimpComponent, {
      width: '600px',
      // data: { item: obj, action: action, category_id: category_id }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        console.log('mailChimpResult: ', result);
        // if (result.event === 'Add') {
        //   console.log('resultDialog: ', result);
        //   this.addItem(result.data);
        // }
        // if (result.event === 'Edit') {
        //   this.editItem(result.data, result.ifImageChange);
        // }
      }
    });
  }


    ngOnDestroy() {
        this.middleService.SharingData.unsubscribe();
    }
}
