/**
 * Created by andrew.yang on 7/28/2017.
 */
import { Injectable } from '@angular/core';
import {BehaviorSubject} from 'rxjs';
import {Product} from '../model/product';
import {Cart} from '../model/cart';

@Injectable()
export class CartService {

    public cartListSubject = new BehaviorSubject([]);
    public toggleCartSubject = new BehaviorSubject(false);

    toggleCart = () => {
        this.toggleCartSubject.next(!this.toggleCartSubject.getValue());
    };
    addToCart = (cart: Cart) => {
        const current = JSON.parse(sessionStorage.getItem('cartItem')) || [];
        const dup = current.find(c => c.product.id === cart.product.id);
        if (dup) { dup.quantity += cart.quantity; } else { current.push(cart); }
        this.cartListSubject.next(current);
        sessionStorage.setItem('cartItem', JSON.stringify(current));
    };
    reloadCart = (cartList) => {
        this.cartListSubject.next(cartList);
      sessionStorage.setItem('cartItem', JSON.stringify(cartList));
    };
    removeCart = index => {
        const current = JSON.parse(sessionStorage.getItem('cartItem'));
        current.splice(index, 1);
        sessionStorage.setItem('cartItem', JSON.stringify(current));
    };
}
