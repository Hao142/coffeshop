/**
 * Created by andrew.yang on 7/27/2017.
 */

import {Injectable} from '@angular/core';
import {Http, Response} from '@angular/http';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import { catchError } from 'rxjs/operators';


@Injectable()
export class ProductService {

    constructor(public http: Http) { }

    public getProducts(dataURL: string) {
        console.log("dataURL: ", dataURL);
        return this.http.get(dataURL)
            .pipe(map((res: Response) => res.json()));
    }
}
