/**
 * Created by andrew.yang on 7/27/2017.
 */
import {CategoryComponent} from './pages/category/category.component';
import {ProductComponent} from './pages/product/product.component';
import {CartPageComponent} from './pages/cart/cart-page.component';
import {EcommerceComponent} from './ecommerce.component';
import { OrderComponent } from './pages/order/order.component';

export const ecommerceRoutes = [
  {
    path: '',
    component: EcommerceComponent,
    children: [
      {
        path: '',
        loadChildren: () => import('./pages/category/category.module').then(m => m.CategoryModule),
        // loadChildren: './pages/category/category.module#CategoryModule'
      },
      {
        path: 'product',
        loadChildren: () => import('./pages/product/product.module').then(m => m.ProductModule),
      },
      {
        path: 'cart',
        loadChildren: () => import('./pages/cart/cart-page.module').then(m => m.CartPageModule),
        // loadChildren: './pages/cart/cart-page.module#CartPageModule'
      },
      {
        path: 'order',
        loadChildren: () => import('./pages/order/order.module').then(m => m.OrderModule),
      },
      {
        path: '**',
        loadChildren: () => import('./pages/category/category.module').then(m => m.CategoryModule),
      }
    ]
  },


    // {
    //     path: '',
    //     component: CategoryComponent,
    //     children: [
    //       {
    //         path: 'product',
    //         component: ProductComponent
    //       },
    //       {
    //         path: 'cart',
    //         component:  CartPageComponent
    //       },
    //       // {
    //       //   path: '**',
    //       //   loadChildren: '/pages/category/category.module#CategoryModule'
    //       // }
    //       { path: '**', loadChildren: () => import('./pages/category/category.module').then(m => m.CategoryModule) }
    //     ]
    // },

];
