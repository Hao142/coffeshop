/**
 * Created by andrew.yang on 7/28/2017.
 */
import { Component, OnInit } from '@angular/core';
import {CartService} from '../../services/cart.service';
import {CategoryApiService} from '../../../services/category-api.service';
import {MiddleServiceService} from '../../services/middle-service.service';
import {FacebookLoginProvider, SocialAuthService} from 'angularx-social-login';

@Component({
    selector: 'top-bar',
    styleUrls: ['./top-bar.component.css'],
    template: `
    <div class="main-header navbar-fixed-top">
        <div class="header-menu">
            <div class="header-mobile-nav-wrapper">
                <button type="button" class="navbar-toggle" (click)="collapse = !collapse">
                    <span class="fa fa-bars fa-2x"></span>
                </button>
            </div>
            <div class="header-logo-wrapper">
                <img class="header-logo-image" src="./assets/imgs/logo.png" alt="Hero">
            </div>
            <div class="header-nav-wrapper">
                <ul class="header-nav">
                    <li class="header-nav-item">
                        <a routerLink="/">Home</a>
                    </li>
                    <li class="header-nav-item">
                        <div class="dropdown">
                          <div>Shop <i class="fa fa-caret-down"></i></div>
                          <div class="dropdown-content">
                            <a *ngFor="let cateogry of categories ;let i = index" (click)="sendCategoryData(cateogry)">
                              {{cateogry.name}}
                            </a>
                          </div>
                        </div>
                    </li>
                    <li class="header-nav-item">
                        <a routerLink='order'>Order</a>
                    </li>
                </ul>
            </div>
            <div class="header-cart-wrapper" style="padding-left: 40%; flex-direction: row; align-items: center; display: flex; border-right-style: groove; text-align: right;">
                <div style="margin-right: 20px; position: relative; display: flex;" *ngIf="this.user == null">
                  <span (click)="signInWithFB()">
                    <mat-icon>facebook</mat-icon>
                  </span>
                </div>
              <div style="margin-right: 20px; position: relative; display: flex; flex-direction: column"
                   *ngIf="this.user" (click)="signOutWithFB()">
                <div>
                    {{this.user.firstName}} {{this.user.lastName}}
                </div>
                <img src="{{ this.user.response.picture.data.url }}" style="margin: auto; border-radius: 40px; width: 50%; height: 50%;">
                </div>
                <div class="header-cart" (click)="toggleCartPopup($event)" style="display: flex">
                    <div class="header-cart-item">
                        <a href="">MY CART
                          <span *ngIf="cart_num">( {{cart_num}} )</span>
                          <span class="fa fa-caret-down"></span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <ul class="mobile-header-nav" *ngIf="collapse" (click)="collapse = !collapse">
            <li>
                <a routerLink="/">Home</a>
            </li>
            <li>
                <a routerLink="/">Shop</a>
            </li>
            <li>
                <a routerLink='order'>Order</a>
            </li>
        </ul>
        <cart-popup></cart-popup>
    </div>
`
})
export class TopbarComponent implements OnInit {
    public isFbLoggedIn: boolean;
    public user: any;
    public categories: any;
    public collapse = false;
    public cart_num: number;
    constructor(
        private productCategoryService: CategoryApiService,
        private authService: SocialAuthService,
        private middleService: MiddleServiceService,
        private cartService: CartService
    ) { }

    ngOnInit() {
        this.user = JSON.parse(sessionStorage.getItem('fbUser'))
        if (this.user == null) {
          // FaceBook login
          this.authService.authState.subscribe((user: any) => {
            this.user = user;
            sessionStorage.setItem('fbUser', JSON.stringify(this.user));
            console.log('this.user: ', this.user);
          });
        }
        // Get Category
        this.productCategoryService.get_list().subscribe((data: any) => {
            this.categories = data.result;
        });

        // Get Order
        this.cartService.cartListSubject
            .subscribe(res => {
                this.cart_num = res.length;
            })
        if (JSON.parse(sessionStorage.getItem('cartItem'))) {
          this.cart_num = JSON.parse(sessionStorage.getItem('cartItem')).length;
          // console.log("cartNum: ", this.cart_num);
        }
    }

    signInWithFB(): void {
      this.authService.signIn(FacebookLoginProvider.PROVIDER_ID);
      this.isFbLoggedIn = true;
    }

    signOutWithFB(): void {
      if (this.isFbLoggedIn == true) {
        this.authService.signOut();
        this.isFbLoggedIn = false;
      }
      window.sessionStorage.clear();
    }

    toggleCartPopup = (event) => {
        event.preventDefault();
        event.stopPropagation();
        this.cartService.toggleCart()
    }

    sendCategoryData(data?: any) {
      this.middleService.SharingData.next(data);
      // console.log('sendData: ', this.middleService.SharingData.next(data));
    }
}
