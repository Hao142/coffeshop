import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { EcommerceComponent } from './ecommerce.component';
import {HttpModule} from '@angular/http';
import {FormsModule} from '@angular/forms';
import {RouterModule} from '@angular/router';
import {ecommerceRoutes} from './ecommerce.routes';
import {TopbarComponent} from './components/topbar/topbar.component';
import {CartService} from './services/cart.service';
import {CartPopupComponent} from './pages/cart/cart-popup/cart-popup.component';
import {ProductService} from './services/products.service';
import {CommonModule} from '@angular/common';
import {SharedModule} from './shared/shared.module';
import {DialogMailChimpComponent} from '../dashboard/component/dialog-mail-chimp/dialog-mail-chimp.component';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material';
import {SocialAuthService, SocialAuthServiceConfig, SocialLoginModule} from 'angularx-social-login';
import {GoogleLoginProvider, FacebookLoginProvider} from 'angularx-social-login';
import {MatIconModule} from '@angular/material/icon';
import { AngularFireModule } from '@angular/fire';
import { AngularFireMessagingModule } from '@angular/fire/messaging';
import { MessagingService } from 'app/services/messaging.service';




@NgModule({
    declarations: [
      TopbarComponent,
      EcommerceComponent,
      CartPopupComponent
    ],
  imports: [
    CommonModule,
    FormsModule,
    HttpModule,
    RouterModule.forChild(ecommerceRoutes),
    SharedModule,
    MatFormFieldModule,
    MatInputModule,
    MatIconModule
  ],
    providers: [
      MessagingService,
      CartService,
      ProductService,
      {
        provide: 'SocialAuthServiceConfig',
        useValue: {
          autoLogin: false,
          providers: [
            {
              id: GoogleLoginProvider.PROVIDER_ID,
              provider: new GoogleLoginProvider(
                'clientId'
              )
            },
            {
              id: FacebookLoginProvider.PROVIDER_ID,
              provider: new FacebookLoginProvider('325884705700878')
            }
          ]
        } as SocialAuthServiceConfig,
      },
      SocialAuthService
      ],
})

// <fb:login-button
// scope="public_profile,email"
// onlogin="checkLoginState();">
//   </fb:login-button>

export class EcommerceModule { }
