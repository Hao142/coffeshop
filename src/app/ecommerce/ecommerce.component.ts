import { Component } from '@angular/core';
import { MessagingService } from 'app/services/messaging.service';
declare const swal: any;

@Component({
  selector: 'app-ecommerce',
  templateUrl: './ecommerce.component.html',
  styleUrls: ['./assets/css/styles.css']
})
export class EcommerceComponent {
  constructor(private messagingService: MessagingService){}
  ngOnInit(){
    this.messagingService.requestPermission();
    this.messagingService.listen();
    this.messagingService.currentMessage.subscribe(message=>{
      if(message && message.data){
        message.data.products = JSON.parse(message.data.products);
        if(message.data.status!=1){
          if(message.data.status == 2 || message.data.status == 3 || message.data.status == 5)
            swal({
              title:'Thông báo',
              text:`Đơn hàng của bạn ${
                message.data.status == 2 ? "đã được xác nhận" :
                message.data.status == 3 ? "đang được vận chuyển" :
                message.data.status == 2 && "đã hết hàng"
              }`,
              confirmButtonClass: 'btn btn-info'
            })
        }
      }
    })
  }
}
