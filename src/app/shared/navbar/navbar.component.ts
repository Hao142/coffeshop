import { Component, OnInit, Input } from '@angular/core';
import {AccountService} from '../../services/account.service';
import {SettingsService} from '../../services/settings.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
  @Input() title: string;
  constructor(
    private settingService: SettingsService,
    private accountService: AccountService
  ) {}

  ngOnInit() {
  }

  menuClick() {
    this.accountService.logout();
  }
}
