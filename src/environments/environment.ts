// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.
const domain = 'https://coffeeshop.smarthome9x.com/api/';
// const domain = 'http://127.0.0.1:8000/api/';
export const environment = {
  productApi: domain + 'product',
  orderApi: domain + 'order',
  categoryApi: domain + 'category',
  userAPI: domain + 'user',
  production: false,
  firebase: {
    apiKey: "AIzaSyC2sSGADD7ddBSqx-8-Tm1bjKc8VAFawr0",
    authDomain: "coffee-54304.firebaseapp.com",
    projectId: "coffee-54304",
    storageBucket: "coffee-54304.appspot.com",
    messagingSenderId: "169648405026",
    appId: "1:169648405026:web:677d0371bf5b81fac4a87b",
    measurementId: "G-DRVJV7F3JH"
  }
};
